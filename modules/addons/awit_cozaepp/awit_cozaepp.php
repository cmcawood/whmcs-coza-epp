<?php
/**
 * AWIT COZAEPP - COZA EPP Module
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we not being accssed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");

// Our global supported field list
$AWIT_COZAEPP_SUPPORTED_FIELDS = array(
	"max_items"
);

// Addon configuration
function awit_cozaepp_config()
{

	// Configuration
	$configarray = array(
		"name" => "AWIT COZAEPP",
		"description" => "This module is to facilitate displaying of epp messages.",
		"version" => "1.2",
		"author" => "AllWorldIT",
		"language" => "english",
		"fields" => array(
			// Admin User
			"max_items" => array (
				"FriendlyName" => "Items Per Page",
				"Description" => "Amount of items to list per page",
				"Type" => "text", "Size" => "30",
				"Default" => "100"
			),
		)
	);

	return $configarray;
}



function awit_cozaepp_get_config_custom_fields()
{
	global $AWIT_COZAEPP_SUPPORTED_FIELDS;

	// Query modules table
	$table = "tbladdonmodules";
	$fields = "setting,value";
	$where = array( 'module' => 'awit_cozaepp' );
	$result = select_query($table,$fields,$where);

	// Filter out the settings we need
	$custom_fields = array();
	while ($row = mysql_fetch_array($result)) {
		// Check in our global list
		if (in_array($row['setting'],$AWIT_COZAEPP_SUPPORTED_FIELDS)) {
			$custom_fields[$row['setting']] = $row['value'];
		}
	}

	return $custom_fields;
}



function _awit_cozaepp_getMaxItems()
{
	$customFields = awit_cozaepp_get_config_custom_fields();
	return $customFields['max_items'];
}



// Addon activation
function awit_cozaepp_activate()
{

	// Create Custom DB Table
	$result = mysql_query("
		CREATE TABLE `mod_awit_cozaepp_messages` (
			`id` INT( 1 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`created` DATETIME NOT NULL,
			`code` VARCHAR(10) NOT NULL,
			`message` TEXT NOT NULL
		)
	");

	// Return Result
	if (!$result) {
		return array("status" => "error", "description"=>"There was a problem activating the module.");
	} else {
		return array("status" => "success","description" =>"Open module configuration for configuration options.");
	}

}



function awit_cozaepp_deactivate()
{
	// Remove custom tables
	$result1 = mysql_query("
		DROP TABLE `mod_awit_cozaepp_messages`
	");

	if (!$result) {
		return array("status"=>"error","description"=>"There was an error deactivating the module.");
	} else {
		return array("status"=>"success","description"=>"Module has been deactivated.");
	}

}



// Addon output
function awit_cozaepp_output($vars)
{
	// Check if we have to display all records
	if (isset($_POST['button']) && $_POST['button'] == 'Display All') {
		$whereClause = '';

	} else if (!empty($_POST['date_to']) && !empty($_POST['date_from'])) {

		// Default dates
		try {
			$dateTo = new DateTime($_POST['date_to']);
			$dateTo = $dateTo->format('Y-m-d');

			$dateFrom = new DateTime($_POST['date_from']);
			$dateFrom = $dateFrom->format('Y-m-d');
		} catch (Exception $ex) {
			// Catching exception against valid date

			$dateFrom = new DateTime(date("Y-m-d"));
			$dateFrom = $dateFrom->format('Y-m-d');

			$dateFrom->modify('+1 day');
			$dateTo = $dateTo->format('Y-m-d');
		}

		$whereClause = "WHERE Date(created) >= Date('".mysql_real_escape_string($dateFrom)."')
				AND Date(created) <= Date('".mysql_real_escape_string($dateTo)."')";
	}

	// Make link to use
	$link = $vars['modulelink'];

	// Fancy date picker
	echo '
		<script>
			$(function() {
				$( "#date_from" ).datepicker({
					dateFormat: "yy-mm-dd",
					constrainInput: true
				});
				$( "#date_to" ).datepicker({
					dateFormat: "yy-mm-dd",
					constrainInput: true
				});
			});
		</script>
	';

	// Date search fields
	echo "<p>Select a start and end date and hit search.</p>";
	echo "<form action='$link' method='post'>";
	echo "<input id='date_from' type='text' value='$dateFrom' name='date_from' />";
	echo "<input id='date_to' type='text' value='$dateTo' name='date_to' />";
	echo "<input type='submit' name='button' value='Search' />";
	echo "<input type='submit' name='button' value='Display All' />";
	echo "<br /><br />";

	$orderClause = 'ORDER BY created DESC';

	// Max amount of records to show per page
	$recordMax = _awit_cozaepp_getMaxItems();

	// Validation
	if (!is_numeric($recordMax)) {
		$recordMax = 100;
	}

	// Setting page number
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	} else if (isset($_POST['page'])) {
		$page = $_POST['page'];
	}

	// Ensuring valid page number
	if ($page < 1) {
		$page = 1;
	}
	// Pagination button handler
	if (isset($_POST['prevPage'])) {
		// Prev Page
		$page = ($page > 1)? ($page - 1) : 1;

	} else if (isset($_POST['nextPage'])) {
		// Next Page
		$page = $page + 1;
	}

	$recordCurrent = intval(abs($recordMax * $page)) - $recordMax;

	$limitClause = "LIMIT $recordCurrent, $recordMax";

	// Query the database, getting the total amount of records
	$result = mysql_query(sprintf("
			SELECT
				COUNT(*) AS cnt
			FROM
				mod_awit_cozaepp_messages
			%s %s
		",
		$whereClause,
		$orderClause
	));

	$row = mysql_fetch_array($result);
	$totalRecords = $row['cnt'];
	$lastPage = ceil($totalRecords / $recordMax);

	// Query the database
	$result = mysql_query(sprintf("
			SELECT
				*
			FROM
				mod_awit_cozaepp_messages
			%s %s %s
		",
		$whereClause,
		$orderClause,
		$limitClause
	));

	// Loop through results and genenrate form
	$includeForm = 0;
	while ($row = mysql_fetch_array($result)) {
		// Open form
		if (!$includeForm) {
			$includeForm = 1;

			echo '<div class="tablebg">';
			echo '<table id="epp-message-log" class="datatable" width="100%" border="0" cellspacing="1" cellpadding="3">';
			echo "<tr>";
			echo "<th>Timestamp</th>";
			echo "<th>Code</th>";
			echo "<th>Message</th>";
			echo "</tr>";
		}

		echo "<tr>";
		echo "<td>".$row['created']."</td>";
		echo "<td>".$row['code']."</td>";
		echo "<td>".$row['message']."</td>";
		echo "</tr>";
	}

	// Close form
	if ($includeForm) {
		echo "<tr><td colspan='5'>";
		if ($page >= $lastPage) {
			echo "<button name='prevPage'> &lt;&lt; Previous Page </button> Page $page";
		} else if ($page == 1) {
			echo " Page $page <button name='nextPage'> Next Page &gt;&gt; </button>";
		} else {
			echo "<button name='prevPage'> &lt;&lt; Previous Page </button> Page $page";
			echo "<button name='nextPage'> Next Page &gt;&gt; </button>";
		}

		echo "</td></tr>";
		echo "</table><br>";
		echo "<input type='hidden' name='page' value='$page'>";
		echo "</form>";
		echo "</div>";
	} else {
		echo "<p>No logs yet for selected period..</p>";
	}

}
